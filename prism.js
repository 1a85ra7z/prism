const fs = require('fs');
const argv = require('yargs').argv;

const lex = require('./js/lex');
const parse = require('./js/parse');

if(argv._[0]) {
    fs.readFile(argv._[0], 'utf-8', (e, d) => {
        if(!e) {
            console.log(
                parse(
                    lex(d)
                )
            );
        }
    });
}