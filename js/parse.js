module.exports = (d) => {
    let r = {
        type: 'prog',
        body: [],
        data: {}
    };

    let cpb = r;

    let p = (dl) => {
        let rv = {
            type: '',
            body: [],
            data: {}
        };

        dl.forEach((dlt, dlti) => {
            if(dlt.type === 'identifier' && dlti === 0) {
                if(dlt.value === 'let') {
                    rv.type = 'declaration';
                    rv.data = {
                        left: {
                            type: 'variable',
                            body: {},
                            data: {}
                        },

                        right: {
                            type: '',
                            body: {},
                            data: {}
                        }
                    }
                }
            }
        });

        cpb.body.push(rv);
    };

    d.forEach(dl => {
        p(dl);
    });

    return r;
};