const ids = require('./identifiers');

module.exports = (d) => {
    return d
        .split('\n')
        .map(s => s.trim())
        .filter(s => s.length)
        .map(s => s.split(/\s/g))
        .map(l => {
            let ct = [];

            l.forEach(clt => {
                ct.push(...(
                    clt.split(/([^\w."'`_:])/g).filter(clti => clti.length).map((ctp, ctpi) => {
                        let ctpt = 'undefined';
                        let ctpv = ctp;

                        if(/^[0-9]+$/.test(ctpv)) {
                            ctpt = 'number';
                        } else if(/\+|-|\*|\/|>|=>|<|<=|=|==/.test(ctpv)) {
                            ctpt = 'operator';
                        } else if(/[(){}\[\]]/.test(ctpv)) {
                            ctpt = 'parenthesis';
                        } else if(/["'`].*["'`]/.test(ctpv)) {
                            ctpt = 'string';
                            ctpv = ctpv.substring(1, ctpv.length - 1);
                        } else if(/[,|;]/.test(ctpv)) {
                            ctpt = 'separator';
                        } else if(ids.includes(ctpv)) {
                            ctpt = 'identifier';
                        } else if(/[a-zA-Z0-9_]*/.test(ctpv)) {
                            ctpt = 'variable';
                        }

                        return {
                            type: ctpt,
                            value: ctpv,
                            length: ctp.length
                        }
                    })
                ));
            });

            return ct;
        });
};